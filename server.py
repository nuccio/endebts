#!/usr/bin/python
"""Flask front-end for endebts.py"""
import os

import config as CONFIG
import secret as SECRET
import endebts

from flask import Flask, render_template, request, redirect, url_for, flash, make_response
# i18n
from flask_babel import Babel

APP = Flask(__name__)
APP.secret_key = SECRET.APP_KEY
BABEL = Babel(APP)

# send translation according to browser header
@BABEL.localeselector
def get_locale():
    """Select locale setting automatically"""
    return request.accept_languages.best_match(CONFIG.LANGUAGES.keys())

# the global variable for storing endebtmentgraphs requested by users
# not reloading them everytime
GLOBALDEBTS = {}

def remove_dupli(listing):
    """Remove duplicates in list"""
    out = []
    for i in listing:
        if i not in out:
            out.append(i)
    return out

def valid_transaction(giver, receiver, description, amount):
    """Check validity of transaction tuple"""
    try:
        amount = float(amount)
    except ValueError:
        amount = 0
    return (giver != ''
            and receiver != []
            and description != ''
            and len(description) < CONFIG.MAXDESCLEN
            and amount != 0)

def format_histo(histo):
    """format history (in reverse and no visible tuples)"""
    history = histo[::-1]
    formated_hist = []
    for line in history:
        rcv_text = ''
        for row in line[4]:
            rcv_text += row + ', '
        rcv_text = rcv_text[:-2]
        formated_hist.append([line[0], line[1], line[2], line[3], rcv_text, line[5]])
    return formated_hist

def round_summary(transacs):
    """rounded summary from transactions"""
    rounded_summary = []
    for item in transacs:
        if round(item[2], 2) != 0:
            rounded_summary.append((item[0], item[1], round(item[2], 2)))
    return rounded_summary

def sort_summary(summary):
    """sort by ower"""
    sorted_summary = sorted(summary, key=lambda debt: debt[1])
    return sorted_summary

def get_equilibrium(summary):
    """check if summary list is empty (evaluates to false)"""
    if summary:
        total = {}
        for transac in summary:
            if transac[0] not in total:
                total.update({transac[0]: 0})
            if transac[1] not in total:
                total.update({transac[1]: 0})
            total[transac[0]] = round(total[transac[0]] + transac[2], 2)
            total[transac[1]] = round(total[transac[1]] - transac[2], 2)

        ordered = sorted(total.items(), key=lambda x: x[1])
        amplitude = max(ordered[-1][1], - ordered[0][1])
        coeffs = []
        for actor_equil in ordered:
            percent = round(100*actor_equil[1]/amplitude)
            if percent < 0:
                percent *= -1
            coeffs.append((actor_equil[0], actor_equil[1], percent))
        return coeffs
    return []

def participants(summary):
    """Generate list of participants from summary"""
    actors = []
    for actor1, actor2, _ in summary:
        if actor1 not in actors:
            actors.append(actor1)
        if actor2 not in actors:
            actors.append(actor2)
    return sorted(actors)

def check_logname(logname):
    """Check validity of history file log name"""
    return logname.isalnum() and len(logname) < CONFIG.MAXLOGNAMELEN

def get_filename(logname):
    """Generate a filepath from requested logname"""
    if CONFIG.PATH == 0:
        #use default path
        folder = os.path.split(os.path.abspath(__file__))[0]
        folder = os.path.join(folder, 'data')
        #create data directory if not present
        if not os.path.isdir(folder):
            os.mkdir(folder)
    else:
        #use config path
        folder = os.path.split(CONFIG.PATH)[0]
    filename = os.path.join(folder, logname + '.csv')
    return folder, filename

def error_in_file(filename):
    """Error message to show if csv history is unreadable"""
    return "<p>Error with history file.</p> \
            <p>Check for possible syntax error.</p> \
            <p>Check the PATH in config.py</p> \
            <p>Check file permissions.</p>"

def get_debt(logname):
    """ Load debt object from global variable
    or create a new one and load it"""
    if logname in GLOBALDEBTS:
        GLOBALDEBTS[logname][0].update()
    else:
        _, filename = get_filename(logname)
        # compute debt graph from file
        debt = endebts.Debts(filename)
        # to add actors later
        added_actors = []
        GLOBALDEBTS.update({logname: [debt, added_actors]})
    return GLOBALDEBTS[logname]

def generate_main(logname):
    """Generate the main page page from template"""
    if check_logname(logname):
        debt, added_actors = get_debt(logname)
        _, filename = get_filename(logname)
        if debt.success:
            summary = round_summary(debt.transacs_simple)
            actors = remove_dupli(participants(summary) + added_actors)
            summary = sort_summary(summary)
            actors = sorted(actors)
            equilibrium = get_equilibrium(summary)
            total_spent = debt.total
            history = format_histo(debt.history)
            #render page
            return render_template('main.html',
                                   name=logname,
                                   money=CONFIG.MONEY,
                                   summary=summary,
                                   history=history,
                                   actors=actors,
                                   equilibrium=equilibrium,
                                   total=total_spent,
                                   logname=logname)
        return error_in_file(filename)
    return "<p>Only alphanumeric characters are allowed.</p>"

@APP.route('/')
def default_main_page():
    """Root of app redirects to logname 'default' """
    return redirect(url_for('main_page', logname='default'))

@APP.route('/<string:logname>')
def main_page(logname):
    """Show main interface relative to logname"""
    return generate_main(logname)

@APP.route('/<string:logname>/add', methods=['POST', 'GET'])
def add_transaction(logname):
    """Listener to add transaction lines in history file"""
    if request.method == 'POST':
        #extract receivers from request
        receivers = [item for item in request.form if request.form[item] == 'on']
        if valid_transaction(request.form['giver'],
                             receivers,
                             request.form['description'],
                             request.form['amount']):
            debt, _ = get_debt(logname)
            #write new transaction
            if debt.success:
                new_trans = (request.form['giver'],
                             tuple(receivers),
                             request.form['amount'])
                added = debt.add(new_trans, request.form['description'],
                                 limit=CONFIG.MAXHISTORYLEN)
                if not added:
                    _, filename = get_filename(logname)
                    return error_in_file(filename)
            #redirect to main view
            return redirect(url_for('main_page', logname=logname))
    # if the request method
    # was GET or invalid transaction
    flash("Invalid Transaction...", 'text-warning')
    #redirect to main view
    return redirect(url_for('main_page', logname=logname))

@APP.route('/<string:logname>/rm_transaction', methods=['POST', 'GET'])
def rm_transaction(logname):
    """Listener to comment-out transaction from history file"""
    if request.method == 'POST':
        # extract line numbers to remove
        lines = [int(item[7:]) for item in request.form if request.form[item] == 'on']
        debt, _ = get_debt(logname)
        if debt.success:
            debt.comment(lines)

    #redirect to main view
    return redirect(url_for('main_page', logname=logname))


@APP.route('/<string:logname>/add_user', methods=['POST', 'GET'])
def add_user(logname):
    """Listener to accept new actors"""
    if request.method == 'POST':
        debt, added_actors = get_debt(logname)
        if (request.form['new_user'] != ''
                and request.form['new_user'] not in debt.actors
                and request.form['new_user'] not in added_actors
                and len(request.form['new_user']) < CONFIG.MAXUSERLEN):
            added_actors.append(request.form['new_user'])
            #redirect to main view
            return redirect(url_for('main_page', logname=logname))
    flash("Invalid username...", 'text-warning')
    #redirect to main view
    return redirect(url_for('main_page', logname=logname))

@APP.route('/<string:logname>/download')
def download_log(logname):
    """Send history file over http"""
    _, filename = get_filename(logname)
    try:
        log = open(filename, 'r', encoding='utf-8')
    except IOError:
        print('Error opening: ' + filename)
        response = redirect(url_for('main_page', logname='default'))
    else:
        response = make_response(log.read())
        response.content_type = "text/plain"
        response.mimetype = "text/plain"
    return response

if __name__ == '__main__':
    APP.debug = True
    APP.run(host='0.0.0.0')
