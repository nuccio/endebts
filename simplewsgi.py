#!/usr/bin/python
from wsgiref.simple_server import make_server
from server import app

HTTPD = make_server('', 5000, app)
print("Serving HTTP on port 5000...")

# Respond to requests until process is killed
HTTPD.serve_forever()
