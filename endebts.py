#!/usr/bin/python
"""Modelize and simplify an endebtment graph using CSV file for storage"""
import time
import csv
import re

###Gestion du fichier d'historique
def generate_graph(histo):
    """Read history file and generate transaction graph"""
    history = None
    success = True
    transacs = []
    transacs_full = []
    #ouvre fichier en lecture
    try:
        history = open(histo, 'r', encoding='utf-8')
    except IOError:
        print('Attempting to create: ' + histo)
        # create it if inexistant
        try:
            history = open(histo, 'a', encoding='utf-8')
        except PermissionError:
            print('Impossible to create history file: ' + histo)
            success = False
        else:
            history.close()
            history = open(histo, 'r', encoding='utf-8')

    if history is not None:
        reader = csv.reader(history, delimiter='\t', quotechar='"')
        try:
            for i, row in enumerate(reader):
                #permet les commentaires et ignore les lignes vides
                if row and row[0] and row[0][0] != "#":
                    row[3] = tuple(re.split(',', row[3]))
                    transacs.append((row[1], row[3], float(row[2])))
                    transacs_full.append([i] + row)
        except IndexError:
            print('Syntax error in ' + histo)
            success = False
        except UnicodeDecodeError:
            print('Unicode decode error in ' + histo)
            success = False
        history.close()
    else:
        success = False
    return transacs, transacs_full, success

def read_all(histo):
    """Read history file and return all lines"""
    #ouvre fichier en lecture
    try:
        history = open(histo, 'r', encoding='utf-8')
    except IOError:
        print('Error reading file:' + histo)
        return False
    reader = csv.reader(history, delimiter='\t', quotechar='"')
    lines = []
    for row in reader:
        lines.append(row)
    history.close()
    return lines

def actors_list(transacs):
    """Generate the list of the participants in the transaction graph"""
    actors = []
    for transac in transacs:
        if transac[0] not in actors:
            actors.append(transac[0])
        if not isinstance(transac[1], tuple):
            if transac[1] not in actors:
                actors.append(transac[1])
        else:
            for actor in transac[1]:
                if actor not in actors:
                    actors.append(actor)
    actors.sort()
    return actors

def compute_total_spent(transacs):
    """Compute the total spent in all transactions"""
    total = 0
    for transac in transacs:
        total += transac[2]
    return round(total, 2)

###Simplification du graph des endebtments
def detect_null(transacs):
    """Select transaction with null amount"""
    for transac in transacs[:]:
        if transac[2] == 0:
            return transac
    return False

def detect_doubl(transacs):
    """Select duplicates transactions with same emitters and receivers"""
    for i in range(len(transacs)):
        for j in range(len(transacs)):
            if i != j:
                transac = transacs[i]
                transac2 = transacs[j]
                #si deux transacs partagent destinataire et emetteur
                if (transac[0], transac[1]) == (transac2[0], transac2[1]):
                    return (transac, transac2, 0)
                #si debt opposée
                elif (transac[0], transac[1]) == (transac2[1], transac2[0]):
                    return (transac, transac2, 1)
    return False

def remove_doubl(doublon, transacs):
    """Remove duplicate transaction"""
    (transac, transac2, operator) = doublon
    if operator == 0:
        transacs.append((transac[0], transac[1], transac[2]+transac2[2]))
    else:
        transacs.append((transac[0], transac[1], transac[2]-transac2[2]))
    transacs.remove(transac)
    transacs.remove(transac2)

def degroup(transacs):
    """Make transactions to be between two actors only"""
    for transac in transacs[:]:
        #si transac destinée à un groupe
        if isinstance(transac[1], tuple):
            #on décompose la transaction
            for i in transac[1]:
                if i != transac[0]:
                    transacs.append((transac[0], i, transac[2]/len(transac[1])))
            transacs.remove(transac)

def detect_cycle(transacs):
    """Select cycle of transactions"""
    for transac_a in transacs:
        for transac_b in transacs:
            for transac_c in transacs:
                if (transac_a != transac_b and transac_a != transac_c
                        and transac_b != transac_c):
                    #si A donne à B, B donne à C et A donne à C
                    if (transac_a[1] == transac_b[0]
                            and transac_b[1] == transac_c[1]
                            and transac_a[0] == transac_c[0]):
                        return (transac_a, transac_b, transac_c)
    return False

def remove_cycle(cycle, transacs):
    """Alter debt graph to remove selected cycle"""
    (transac_a, transac_b, transac_c) = cycle
    if transac_b[2] < transac_c[2]:
        transacs.append((transac_a[0], transac_a[1], transac_a[2]-transac_b[2]))
        transacs.append((transac_c[0], transac_c[1], transac_c[2]+transac_b[2]))
    else:
        transacs.append((transac_a[0], transac_a[1], transac_a[2]+transac_c[2]))
        transacs.append((transac_b[0], transac_b[1], transac_b[2]+transac_c[2]))
    transacs.remove(transac_a)
    transacs.remove(transac_b)
    transacs.remove(transac_c)

def detect_cascad(transacs):
    """Select transactions going A -> B -> C"""
    for transac in transacs:
        for transac2 in transacs:
            if transac != transac2:
                if transac[1] == transac2[0]:
                    return (transac, transac2)
    return False

def remove_cascad(cascade, transacs):
    """Alter graph to remove cascade"""
    (transac_d, transac_e) = cascade
    if transac_d[2] > transac_e[2]:
        transacs.append((transac_d[0], transac_e[1], transac_e[2]))
        transacs.append((transac_d[0], transac_d[1], transac_d[2]-transac_e[2]))
    else:
        transacs.append((transac_d[0], transac_e[1], transac_d[2]))
        transacs.append((transac_d[1], transac_e[1], transac_e[2]-transac_d[2]))
    transacs.remove(transac_d)
    transacs.remove(transac_e)

def detect_neg(transacs):
    """Select transactions with amount < 0"""
    for transac in transacs[:]:
        if transac[2] < 0:
            return transac
    return False

def remove_neg(transac, transacs):
    """Remove negative by reversing it"""
    transacs.append((transac[1], transac[0], -transac[2]))
    transacs.remove(transac)

def detect_cross(transacs):
    """detect person giving to two persons"""
    double_givers = []
    for i in range(len(transacs)):
        for j in range(len(transacs)):
            if i != j:
                transac = transacs[i]
                transac2 = transacs[j]
                if transac[0] == transac2[0] and transac[1] != transac2[1]:

                    if ([transac, transac2] not in double_givers and
                            [transac2, transac] not in double_givers):

                        double_givers.append([transac, transac2])

    #detect cross between double givers
    for i in range(len(double_givers)):
        for j in range(len(double_givers)):
            if i != j:
                dgi = double_givers[i]
                dgj = double_givers[j]

                couple_a = dgi[0][1], dgi[1][1]
                couple_b = dgj[0][1], dgj[1][1]

                if couple_a == couple_b:
                    return [dgi[0], dgi[1], dgj[0], dgj[1]]
    return False

def remove_cross(cross, transacs):
    """remove crossing transactions"""
    for transac in cross:
        transacs.remove(transac)

    cross = sorted(cross, key=lambda x: x[2])
    minimal = cross[0]
    cross.remove(minimal)

    for transac in cross:
        if minimal[0] != transac[0] and minimal[1] != transac[1]:
            crossed = transac
    cross.remove(crossed)

    #replace them with minimal transaction distributed on others
    transacs.append((crossed[0], crossed[1], crossed[2] - minimal[2]))
    transacs.append((cross[0][0], cross[0][1], cross[0][2] + minimal[2]))
    transacs.append((cross[1][0], cross[1][1], cross[1][2] + minimal[2]))


def simplify(transacs):
    """Simplification algorithm to apply on endebtment graph"""
    degroup(transacs)
    while True:
        # must be first for speed optimization (most frequent first)
        d_doubl = detect_doubl(transacs)
        if d_doubl != False:
            remove_doubl(d_doubl, transacs)
            continue
        d_nul = detect_null(transacs)
        #s'il existe une transaction nulle
        if d_nul != False:
            transacs.remove(d_nul)
            continue
        d_neg = detect_neg(transacs)
        if d_neg != False:
            remove_neg(d_neg, transacs)
            continue
        d_casc = detect_cascad(transacs)
        if d_casc != False:
            remove_cascad(d_casc, transacs)
            continue
        d_cycle = detect_cycle(transacs)
        if d_cycle != False:
            remove_cycle(d_cycle, transacs)
            continue
        d_cross = detect_cross(transacs)
        if d_cross != False:
            remove_cross(d_cross, transacs)
            continue
#la simplification est terminée:
#il ne reste plus de debts nulles, négatives, redondantes,
#cycliques, en cascade ou croisées
        break

class Debts:
    """Object describing endebtment graph and methods to alter it"""
    def __init__(self, histo):
        """Load history file and process it"""
        self.historyname = histo
        self.update()

    def update(self):
        """Generate graph and simplify it"""
        transacs, self.history, self.success = generate_graph(self.historyname)
        if self.success:
            self.actors = actors_list(transacs)
            self.total = compute_total_spent(transacs)
            self.transacs_simple = transacs

            simplify(self.transacs_simple)

    def add(self, transac, description, dateandtime=None, limit=None):
        """Add new transaction to history file"""
        #le temps par défaut est le temps local
        if dateandtime is None:
            dateandtime = time.localtime()
        #Détail de formatage
        if isinstance(transac[1], tuple):
            destinataires = ""
            for i in transac[1]:
                destinataires += i
                destinataires += ","
            destinataires = destinataires[:-1]    #retire virgule finale
        else:
            destinataires = transac[1]
        #ajout de la transaction dans le format csv
        #si destinataire différent d'émetteur et montant non nul
        if transac[0] != destinataires and transac[2] != 0.0:
            transac = (time.strftime('%d/%m/%y %H:%M', dateandtime),
                       transac[0], transac[2], destinataires, description)

            try:
                history_file = open(self.historyname, 'r', encoding='utf-8')
            except IOError:
                print('Error reading file:' + self.historyname)
                return False
            else:
                row_count = sum(1 for row in history_file)

            try:
                history_file = open(self.historyname, 'a', encoding='utf-8')
            except IOError:
                print('Error opening file:' + self.historyname)
                return False
            else:
                if limit is None or row_count < limit:
                    writer = csv.writer(history_file, delimiter='\t', quotechar='"')
                    writer.writerow(transac)
                    history_file.close()
                    self.update()
                    return True
                history_file.close()
                return False
        return False

    def comment(self, line_nbs):
        """Add "#" before transaction line in history file to ignore it
        while keeping it"""
        full_histo = read_all(self.historyname)
        new_histo = []
        for i, line in enumerate(full_histo):
            #comment line if notified
            if i in line_nbs:
                new_histo.append(['#--'] + line)
            else:
                #copy
                new_histo.append(line)
        #write commented version
        historique = open(self.historyname, 'w', encoding='utf-8')
        writer = csv.writer(historique, delimiter='\t', quotechar='"')
        writer.writerows(new_histo)
        historique.close()
        self.update()


if __name__ == '__main__':
    pass
